package com.bradesco.poc.model

import java.util.*

data class Transaction(
        val date: Date,
        val description: String,
        val amount: Float)

