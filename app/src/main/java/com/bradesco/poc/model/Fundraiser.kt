package com.bradesco.poc.model

data class Fundraiser(
        val id: Int,
        val name: String,
        val description: String,
        val goal: Float,
        val reached: Float,
        val account: Account
)

val fundraisers = listOf(
        Fundraiser(
                0,
                "JuntAí! para Aniversário",
                "",
                1000f, 10f,
                Account("3684", "103")),
        Fundraiser(
                1,
                "JuntAí! para Viagem",
                "",
                5000f, 1000f,
                Account("3684", "103")),
        Fundraiser(
                2,
                "JuntAí! para Churrasco",
                "",
                500f,
                500f,
                Account("3684", "103")),
        Fundraiser(
                3,
                "JuntAí! para Doação",
                "",
                2000f,
                100f,
                Account("3684", "103"))
)
