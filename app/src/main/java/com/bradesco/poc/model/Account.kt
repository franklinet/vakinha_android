package com.bradesco.poc.model

data class Account(val branch: String, val number: String)