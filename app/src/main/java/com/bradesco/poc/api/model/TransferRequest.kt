package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class TransferRequest(
        @SerializedName("recipientCodeBranch") val recipientBranch: String,
        @SerializedName("recipientAccountNumber") val recipientAccount: String,
        @SerializedName("description") val description: String,
        @SerializedName("amount") val amount: Float
)
