package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class OtpResponse(
    @SerializedName("codeOTP") val otp: String = ""
)
