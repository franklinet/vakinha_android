package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class StatusResponse(
        @SerializedName("status") var status: String = ""
)