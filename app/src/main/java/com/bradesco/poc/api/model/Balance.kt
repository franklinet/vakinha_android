package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class Balance (
    @SerializedName("checkingAccount") val checkingAccount: CheckingAccount? = null
)