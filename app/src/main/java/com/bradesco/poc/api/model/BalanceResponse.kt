package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class BalanceResponse(
    @SerializedName("balance") val balance: Balance? = null
)