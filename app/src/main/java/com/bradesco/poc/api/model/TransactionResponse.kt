package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class TransactionResponse(
    @SerializedName("transactions") var transactions: List<Transaction> = listOf()
)