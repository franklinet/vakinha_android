package com.bradesco.poc.api

import com.bradesco.poc.api.model.BalanceResponse
import com.bradesco.poc.api.model.OtpResponse
import com.bradesco.poc.api.model.StatusResponse
import com.bradesco.poc.api.model.TransactionResponse
import com.bradesco.poc.api.model.TransferRequest
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.Request
import okhttp3.RequestBody
import kotlin.concurrent.thread

class ApiException(message: String) : Exception(message)
private const val BASE_URL = "https://lab6.prebanco.com.br:8443/v2/"
private fun endpoint(path: String): String {
    require(path.isNotBlank()) { "path must not be empty or blank" }
    return BASE_URL + path
}

private inline fun <reified T> Response.fromJson(): T {
    val json = body()?.string() ?:
        throw ApiException("Error: empty server response")
    return Gson().fromJson<T>(json, T::class.java)
}

private fun Response.apiException(): ApiException {
    return ApiException("Error: ${code()} ${message()}")
}

object BankApi {

    fun getUserBalance(
            token: String,
            onSuccess: (BalanceResponse?) -> Unit,
            onFailure: (Exception) -> Unit
    ) {
        val call = Request.Builder()
                .url(endpoint("accounts/balance"))
                .get()
                .header("Authorization", "Bearer $token")
                .build().let {
                    untrustedOkHttpClient.newCall(it)
                }

        thread {
            try {
                val response = call.execute()
                if (response.isSuccessful) {
                    onSuccess(response.fromJson<BalanceResponse>())
                } else {
                    onFailure(response.apiException())
                }
            } catch (exception: Exception) {
                onFailure(exception)
            }
        }
    }

    fun getUserTransactions(
            token: String,
            onSuccess: (TransactionResponse?) -> Unit,
            onFailure: (Exception) -> Unit
    ) {
        val call = Request.Builder()
                .url(endpoint("accounts/lastTransactions"))
                .get()
                .header("Authorization", "Bearer $token")
                .build().let {
                    untrustedOkHttpClient.newCall(it)
                }

        thread {
            try {
                val response = call.execute()
                if (response.isSuccessful) {
                    onSuccess(response.fromJson<TransactionResponse>())
                } else {
                    onFailure(response.apiException())
                }
            } catch (exception: Exception) {
                onFailure(exception)
            }
        }
    }

    fun generateOtp(
            token: String,
            onSuccess: (String) -> Unit,
            onFailure: (Exception) -> Unit
    ) {
        val call = Request.Builder()
                .url("https://lab6.prebanco.com.br:8443/auth/generateOTP")
                .get()
                .header("Authorization", "Bearer $token")
                .build().let {
                    untrustedOkHttpClient.newCall(it)
                }

        thread {
            try {
                val response = call.execute()
                if (response.isSuccessful) {
                    onSuccess(response.fromJson<OtpResponse>().otp)
                } else {
                    onFailure(response.apiException())
                }
            } catch (exception: Exception) {
                onFailure(exception)
            }
        }
    }

    fun transferMoney(
            request: TransferRequest,
            token: String,
            otp: String,
            onSuccess: () -> Unit,
            onFailure: (Exception) -> Unit
    ) {
        val json = Gson().toJson(request)

        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        val call = Request.Builder()
                .url(endpoint("transactions/moneyTransfer"))
                .post(requestBody)
                .header("Authorization", "Bearer $token")
                .header("X-OTP", otp)
                .build().let {
                    untrustedOkHttpClient.newCall(it)
                }

        thread {
            try {
                val response = call.execute()
                when {
                    response.code() == 204 -> onSuccess()
                    response.code() == 200 -> {
                        val status = response.fromJson<StatusResponse>()
                        onFailure(ApiException(status.status))
                    }
                    else -> onFailure(ApiException("Unknown error response: ${response.code()}"))
                }
            } catch (exception: Exception) {
                onFailure(exception)
            }
        }
    }
}

