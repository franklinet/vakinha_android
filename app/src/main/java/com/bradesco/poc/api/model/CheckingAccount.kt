package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class CheckingAccount(
    @SerializedName("value") val value: Float? = null
)