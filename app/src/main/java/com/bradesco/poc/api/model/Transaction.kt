package com.bradesco.poc.api.model

import com.google.gson.annotations.SerializedName

data class Transaction(
    @SerializedName("date") var date: String? = null,
    @SerializedName("description") var description: String = "",
    @SerializedName("amount") var amount: Float = 0f
)