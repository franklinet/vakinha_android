package com.bradesco.poc.adapter

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bradesco.poc.R
import com.bradesco.poc.extensions.formatAsCurrency
import com.bradesco.poc.model.Transaction
import kotlinx.android.synthetic.main.item_transaction_day_header.view.*
import kotlinx.android.synthetic.main.item_transaction_item.view.*
import kotlinx.android.synthetic.main.item_transaction_month_header.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

private val DAY_FORMAT = SimpleDateFormat("dd/MM", Locale.getDefault())
private val MONTH_FORMAT = SimpleDateFormat("MMMM", Locale.getDefault())
private const val DAY_HEADER = 0
private const val MONTH_HEADER = 2
private const val TRANSACTION_ITEM = 3
data class DayListItem(val date: Date)
data class MonthListItem(val date: Date)
data class TransactionListItem(val summary: String, val amount: Float)
class TransactionListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mList: List<Any> = listOf()

    var transactions: List<Transaction> by Delegates.observable(listOf()) { _, _, _ ->
        onTransactionListChanged()
    }

    var size: Int by Delegates.observable(-1) { _, _, _ ->
        onTransactionListChanged()
    }

    var displayMonth: Boolean by Delegates.observable(true) { _, _, _ ->
        onTransactionListChanged()
    }

    private fun Date.isSameDay(other: Date): Boolean {
        val thisCal = Calendar.getInstance()
        val otherCal = Calendar.getInstance()
        thisCal.time = this
        otherCal.time = other
        return thisCal.get(Calendar.YEAR) == otherCal.get(Calendar.YEAR)
                && thisCal.get(Calendar.DAY_OF_YEAR) == otherCal.get(Calendar.DAY_OF_YEAR)
    }

    private fun Date.isSameMonth(other: Date): Boolean {
        val thisCal = Calendar.getInstance()
        val otherCal = Calendar.getInstance()
        thisCal.time = this
        otherCal.time = other
        return thisCal.get(Calendar.YEAR) == otherCal.get(Calendar.YEAR)
                && thisCal.get(Calendar.MONTH) == otherCal.get(Calendar.MONTH)
    }

    private fun onTransactionListChanged() {
        val adapterList = mutableListOf<Any>()
        var currentDay = Date(0)
        var currentMonth = Date(0)
        transactions.asSequence().sortedByDescending {
            it.date.time
        }.let {
            if (size >= 0) it.take(size) else it
        }.forEach {
            if (!currentMonth.isSameMonth(it.date) && displayMonth) {
                adapterList += MonthListItem(it.date)
                currentMonth = it.date
            }
            if (!currentDay.isSameDay(it.date)) {
                adapterList += DayListItem(it.date)
                currentDay = it.date
            }
            adapterList += TransactionListItem(it.description, it.amount)
        }
        mList = adapterList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when(mList[position]) {
            is DayListItem -> DAY_HEADER
            is MonthListItem -> MONTH_HEADER
            is TransactionListItem -> TRANSACTION_ITEM
            else -> throw IllegalArgumentException("Invalid object for adapter")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when(viewType) {
            DAY_HEADER -> {
                val view = inflater.inflate(R.layout.item_transaction_day_header, parent, false)
                DayViewHolder(view)
            }
            MONTH_HEADER -> {
                val view = inflater.inflate(R.layout.item_transaction_month_header, parent, false)
                MonthViewHolder(view)
            }
            TRANSACTION_ITEM -> {
                val view = inflater.inflate(R.layout.item_transaction_item, parent, false)
                TransactionItemViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid viewType for adapter")
        }
    }

    override fun getItemCount() = mList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DayViewHolder -> {
                holder.day.text = DAY_FORMAT.format((mList[position] as DayListItem).date)
                if (displayMonth) {
                    holder.day.setTextColor(Color.BLACK)
                }
            }
            is MonthViewHolder -> {
                holder.month.text = MONTH_FORMAT.format((mList[position] as MonthListItem).date).capitalize()
            }
            is TransactionItemViewHolder -> {
                val item = mList[position] as TransactionListItem
                holder.summary.text = item.summary
                holder.amount.text = item.amount.formatAsCurrency()
                if (item.amount >= 0) {
                    holder.amount.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.cognizantGreen))
                } else {
                    holder.amount.setTextColor(Color.RED)
                }
            }

        }
    }

    class DayViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val day: TextView = itemView.date_text
    }

    class MonthViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val month: TextView = itemView.month
    }

    class TransactionItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val summary: TextView = itemView.summary
        val amount: TextView = itemView.amount
    }

}