package com.bradesco.poc.modules.vaquinhalist;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bradesco.poc.R;
import com.bradesco.poc.extensions.CurrencyExtensionsKt;
import com.bradesco.poc.model.Fundraiser;

import java.util.List;

public class VaquinhaListAdapter extends RecyclerView.Adapter<VaquinhaListAdapter.ViewHolder>  {
    private List<Fundraiser> mVaquinhas;
    private ItemClickListener mClickListener;

    public VaquinhaListAdapter(List<Fundraiser> vaquinhas) {
        this.mVaquinhas = vaquinhas;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.vaquinha_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Fundraiser model = mVaquinhas.get(position);
        holder.setTitle(model.getName());
        holder.setdescricao(model.getDescription());
        holder.setMontante("Objetivo: " + CurrencyExtensionsKt.formatAsCurrency(model.getGoal()));
        holder.setOnClickListener(mClickListener);
    }

    @Override
    public int getItemCount() {
        return mVaquinhas.size();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnClickListener(ItemClickListener listener) {
        mClickListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mIndicatorImageView;
        private TextView mTitleTextView;
        private TextView mDescricaoTextView;
        private TextView mMontanteTextView;
        private boolean mExpanded = false;
        private ConstraintLayout mRootView;
        private Button mAttemptButton;

        ViewHolder(View itemView) {
            super(itemView);
            mRootView = (ConstraintLayout) itemView;

            mIndicatorImageView = itemView.findViewById(R.id.indicatorImageView);
            mTitleTextView = itemView.findViewById(R.id.titleTextView);
            mDescricaoTextView = itemView.findViewById(R.id.descricaoTextView);
            mMontanteTextView = itemView.findViewById(R.id.montanteTextView);
            mAttemptButton = itemView.findViewById(R.id.attemptButton);

            itemView.setOnClickListener(this);
        }

        public void setTitle(String title) {
            mTitleTextView.setText(title);
        }

        public void setdescricao(String descricao) {
            mDescricaoTextView.setText(descricao);
        }

        public void setMontante(String montante) {
            mMontanteTextView.setText(montante);
        }

        public void setOnClickListener(final ItemClickListener listener) {
            mAttemptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(v, getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {
            expandAnimation(mExpanded);
        }

        private void expandAnimation(boolean isExpanded) {
            ChangeBounds changeBounds = new ChangeBounds();
            changeBounds.setInterpolator(new OvershootInterpolator());

            TransitionManager.beginDelayedTransition(mRootView, changeBounds);

            int height = isExpanded ? 1 : ConstraintLayout.LayoutParams.WRAP_CONTENT;

            ConstraintSet expandConstraint = new ConstraintSet();
            expandConstraint.clone(mRootView);
            expandConstraint.constrainHeight(R.id.expandableConstraintLayout, height);
            expandConstraint.applyTo(mRootView);

            int degrees = isExpanded ? 0 : -180;

            mIndicatorImageView.animate().rotation(degrees).start();

            mExpanded = !isExpanded;
        }
    }
}
