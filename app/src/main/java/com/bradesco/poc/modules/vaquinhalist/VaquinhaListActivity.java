package com.bradesco.poc.modules.vaquinhalist;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bradesco.poc.R;
import com.bradesco.poc.activity.FundraiserActivity;
import com.bradesco.poc.activity.Extras;
import com.bradesco.poc.model.FundraiserKt;
import com.bradesco.poc.modules.banksList.ChooseBankActivity;

import java.util.ArrayList;
import java.util.List;

public class VaquinhaListActivity extends AppCompatActivity implements VaquinhaListAdapter.ItemClickListener {
    VaquinhaListAdapter mAdapter;
    RecyclerView mList;
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaquinha_list);

        mAdapter = new VaquinhaListAdapter(FundraiserKt.getFundraisers());
        mAdapter.setOnClickListener(this);

        mLayoutManager = new LinearLayoutManager(this);

        mList = findViewById(R.id.vaquinhaRecyclerView);

        mList.setLayoutManager(mLayoutManager);
        mList.setAdapter(mAdapter);

        DividerItemDecoration divider = new DividerItemDecoration(this,mLayoutManager.getOrientation());
        divider.setDrawable(getResources().getDrawable(R.drawable.list_divider_space));

        mList.addItemDecoration(divider);

        addTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private  void addTitle() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Lista de Crowdfundings");
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent bankChooserIntent = new Intent(this, ChooseBankActivity.class);
        bankChooserIntent.putExtra(Extras.FUNDRAISER_ID_EXTRA,
                FundraiserKt.getFundraisers().get(position).getId());
        startActivity(bankChooserIntent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
