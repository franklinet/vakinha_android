package com.bradesco.poc.modules.bankAutentication;

import android.util.Log;
import android.webkit.JavascriptInterface;

public class MyJavaScriptInterface {
    public interface MyJavaScriptInterfaceListener {
        void onReceiveHtml(String html);
    }

    private MyJavaScriptInterfaceListener listener;

    public void setListener(MyJavaScriptInterfaceListener listener) {
        this.listener = listener;
    }

    MyJavaScriptInterface() {
    }

    @JavascriptInterface
    public  void html(String html) {
        listener.onReceiveHtml(html);
    }
}
