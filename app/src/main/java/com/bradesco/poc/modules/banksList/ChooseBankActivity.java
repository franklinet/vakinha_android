package com.bradesco.poc.modules.banksList;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.bradesco.poc.R;
import com.bradesco.poc.activity.Extras;
import com.bradesco.poc.activity.FundraiserActivity;
import com.bradesco.poc.activity.SwipeBackActivity;

public class ChooseBankActivity extends SwipeBackActivity implements View.OnClickListener {
    private Spinner mSpinner;

    private int fundraiserId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_bank);

        mSpinner = findViewById(R.id.spinner_bank_choose);
        mSpinner.setAdapter(createSpinnerAdapter());

        fundraiserId = getIntent().getExtras().getInt(Extras.FUNDRAISER_ID_EXTRA, -1);

        addTitle();
    }

    private SpinnerAdapter createSpinnerAdapter() {
        String[] banks = new String[5];
        banks[0] = "Selecione seu banco";
        banks[1] = "Bradesco";
        banks[2] = "Banco do Brasil";
        banks[3] = "Banco Original";
        banks[4] = "Sicoob";

        return new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, banks);
    }

    private  void addTitle() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Escolha seu Banco");
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {

        if (mSpinner.getSelectedItemPosition() == 1) {
            Intent intent = new Intent("com.cognizant.bradesco.modules.AUTHENTICATION");
            intent.putExtra("app_name", "JuntAí");
            intent.putExtra("app_packageName", "com.bradesco.poc");
            intent.setType("text/plan");

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, 1);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Atenção!");
                alert.setMessage("Instale o aplicativo Bradesco para acessar sua conta");
                alert.setCancelable(true);
                alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                    }
                });
                alert.show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 1) {
            Intent fundraiserIntent = new Intent(this, FundraiserActivity.class);
            fundraiserIntent.putExtras(data);
            fundraiserIntent.putExtra(Extras.FUNDRAISER_ID_EXTRA, fundraiserId);
            startActivity(fundraiserIntent);
            finish();
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }

        if (resultCode == RESULT_CANCELED) {
            String message = "Cancelado pelo usuário";
            if (data != null && data.getExtras() != null) {
                message =
                    data.getExtras().getString("AUTH_ERROR_MESSAGE", message);
            }

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Atenção!");
            alert.setMessage(message);
            alert.setCancelable(true);
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                }
            });
            alert.show();
        }
    }
}
