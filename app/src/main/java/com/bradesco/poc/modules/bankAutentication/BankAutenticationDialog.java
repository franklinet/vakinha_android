package com.bradesco.poc.modules.bankAutentication;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bradesco.poc.R;
import com.google.gson.Gson;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class BankAutenticationDialog extends Dialog implements MyJavaScriptInterface.MyJavaScriptInterfaceListener {
    public interface BankAutenticationDialogListener {
        void onFailure(String message);
        void onAutenticated(String token);
    }

    private BankAutenticationDialogListener mListener;

    private WebView mBankAutentication;

    public BankAutenticationDialog(@NonNull Context context) {
        super(context, R.style.DialogbankAutenticationTheme);
        commonInit();
    }

    public BankAutenticationDialog(@NonNull Context context, int themeResId) {
        super(context, R.style.DialogbankAutenticationTheme);
        commonInit();
    }

    private void commonInit() {
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_bank_autentication);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        MyJavaScriptInterface javaScriptInterface = new MyJavaScriptInterface();
        javaScriptInterface.setListener(this);

        mBankAutentication = findViewById(R.id.wv_bank);
        mBankAutentication.setWebViewClient(sslWebViewClient());
        mBankAutentication.addJavascriptInterface(javaScriptInterface, "JSinterface");
        mBankAutentication.getSettings().setJavaScriptEnabled(true);

    }

    public void setListener(BankAutenticationDialogListener listener) {
        mListener = listener;
    }

    @Override
    public void show() {
        super.show();
        loadBankAutentication();
    }

    private void loadBankAutentication() {
        mBankAutentication.loadUrl("https://lab6.prebanco.com.br:8443/v1/oauth/authorize?clientId=54f0c455-4d80-421f-82ca-9194df24859d&responseType=code&redirectUri=https://lab6.prebanco.com.br:8443/oauth/v2/client/authcode?auth=done&scope=saldo ");
    }

    private WebViewClient sslWebViewClient() {
        return new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                view.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                super.onPageFinished(view, url);
                view.loadUrl("javascript:window.JSinterface.html('<html>' + document.documentElement.innerHTML + '</hmtl>' )");
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        };
    }

    @Override
    public void onReceiveHtml(String html) {
        validadeHTML(html);
    }

    private void validadeHTML(String html) {
        Document document = Jsoup.parse(html);
        final Elements pres = document.getElementsByTag("pre");

        final int visibility = pres.size() == 0 ? View.VISIBLE : View.INVISIBLE;
        mBankAutentication.post(new Runnable() {
            @Override
            public void run() {
                mBankAutentication.setVisibility(visibility);
            }
        });

        if (visibility == View.INVISIBLE) {
            createJsonToken(pres.first().text());
        }
    }

    private void createJsonToken(String jsonToken) {

        if (jsonToken.length() == 0) {
            mListener.onFailure("Operação cancelada!");
            return;
        }

        if (jsonToken != null) {
            mListener.onAutenticated(jsonToken);
        }
    }

}
