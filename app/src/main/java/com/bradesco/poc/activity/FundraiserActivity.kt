package com.bradesco.poc.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.LayoutInflater
import android.widget.ScrollView
import com.bradesco.poc.R
import com.bradesco.poc.model.*
import com.bradesco.poc.presenter.FundraiserActivityPresenter
import com.bradesco.poc.extensions.formatAsCurrency
import kotlinx.android.synthetic.main.activity_fundraiser.*
import kotlinx.android.synthetic.main.dialog_donate.view.*

class FundraiserActivity : SwipeBackActivity() {

    private val presenter = FundraiserActivityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fundraiser)
        supportActionBar?.title = "Página do JuntAí!"

        presenter.attach(this)

        balance_view.onPaymentHistoryClick = presenter::showPaymentHistory
        fundraiser_view.onPaymentHistoryClick = presenter::showPaymentHistory
        fundraiser_view.onDonationClicked = this::onDonationClicked
        fundraiser_view.onCheckOthersClicked = presenter::checkOther
        header_view.onConfigurationClick = presenter::onAccountConfigurationClick
        header_view.onAccountChangeClick = presenter::onAccountChangeClick
    }

    override fun onResume() {
        super.onResume()
        presenter.checkStatus()
    }

    fun onDonationClicked(amount: Float) {

        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.dialog_donate, null)
        val dialog = AlertDialog.Builder(this).run {
            setView(view)
            create()
        }.also { it.show() }

        view.donate_amount_label.text = amount.formatAsCurrency()

        view.continue_btn.setOnClickListener {
            dialog.dismiss()
            presenter.donate(amount)
        }

        view.cancel_btn.setOnClickListener {
            dialog.dismiss()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    fun setUser(userName: String, account: String, branch: String) {
        header_view.userName = userName
        header_view.account = account
        header_view.branch = branch
    }

    fun setFundraiser(name: String, description: String, goal: Float, reached: Float) {
        fundraiser_view.let {
            it.name = name
            it.description = description
            it.goal = goal
            it.reached = reached
        }
    }

    fun setBalance(balance: Float, checkingBalance: Float, availableBalance: Float) {
        balance_view.balance = balance
        balance_view.checkingBalance = checkingBalance
        balance_view.availableBalance = availableBalance
    }

    fun setTransactionList(transactions: List<Transaction>) {
        balance_view.transactions = transactions
        balance_view.displayMonth = false
    }

    fun setPaymentSuccess() {
        fundraiser_view.setPaymentSuccess()
        scrollDown()
    }

    fun setLoading(loading: Boolean) {
        scrollView.visibility = if (loading) View.GONE else View.VISIBLE
        header_view.visibility = if (loading) View.GONE else View.VISIBLE
        activityProgressBar.visibility = if (!loading) View.GONE else View.VISIBLE
    }

    private fun scrollDown() {
        Handler().postDelayed({
            scrollView.fullScroll(ScrollView.FOCUS_DOWN)
        }, 200)
    }
}
