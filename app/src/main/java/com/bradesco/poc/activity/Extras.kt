package com.bradesco.poc.activity

object Extras {
    const val FUNDRAISER_ID_EXTRA = "fundraiser_id"
    const val USER_ACCOUNT_NAME = "user_account_name"
    const val ACCOUNT_NUMBER_EXTRA = "account_number"
    const val BRANCH_NUMBER_EXTRA = "branch_number"
    const val ACCESS_TOKEN_EXTRA = "access_token"
}
