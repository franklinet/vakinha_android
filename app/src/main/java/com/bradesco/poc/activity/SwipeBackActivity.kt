package com.bradesco.poc.activity

import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.view.GestureDetector
import android.view.MenuItem
import android.view.MotionEvent
import com.bradesco.poc.R
import kotlin.math.abs

open class SwipeBackActivity : AppCompatActivity() {
    private lateinit var gestureDetector: GestureDetectorCompat

    override fun onTouchEvent(event: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (gestureDetector.onTouchEvent(event)) {
            return true
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        gestureDetector = GestureDetectorCompat(this,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onFling(
                            event1: MotionEvent,
                            event2: MotionEvent,
                            velocityX: Float,
                            velocityY: Float): Boolean {

                        val dx = event2.x - event1.x
                        val dy = event2.y - event1.y
                        return if (abs(dy) < abs(dx)
                                && dx > 150
                                && abs(velocityX) > 200
                                && abs(velocityX) > abs(velocityY)) {
                            finish()
                            true
                        } else false
                    }
                })
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
