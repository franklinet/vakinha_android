package com.bradesco.poc.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bradesco.poc.R
import com.bradesco.poc.adapter.TransactionListAdapter
import com.bradesco.poc.model.Transaction
import com.bradesco.poc.presenter.FundraiserHistoryActivityPresenter
import kotlinx.android.synthetic.main.activity_fundraiser_history.*

class FundraiserHistoryActivity : AppCompatActivity() {

    private val adapter = TransactionListAdapter().also {
        it.displayMonth = true
    }

    private val presenter = FundraiserHistoryActivityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fundraiser_history)
        resizeWindow()

        presenter.attach(this)

        close_btn.setOnClickListener { finish() }

        history_recycler.adapter = adapter
        history_recycler.layoutManager = LinearLayoutManager(this)
    }

    private fun resizeWindow() {
        val metrics = resources.displayMetrics
        val screenWidth = (metrics.widthPixels * 0.96).toInt()
        val screenHeight = (metrics.heightPixels * 0.70).toInt()

        window.setLayout(screenWidth, screenHeight)
    }

    fun setStatements(statements: List<Transaction>) {
       adapter.transactions = statements
    }

    fun setLoading(loading: Boolean) {
        history_recycler.visibility = if (loading) View.GONE else View.VISIBLE
        activityProgressBar.visibility = if (!loading) View.GONE else View.VISIBLE
    }
}