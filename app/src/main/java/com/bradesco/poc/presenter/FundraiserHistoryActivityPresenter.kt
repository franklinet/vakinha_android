package com.bradesco.poc.presenter

import android.widget.Toast
import com.bradesco.poc.activity.FundraiserHistoryActivity
import com.bradesco.poc.activity.Extras
import com.bradesco.poc.api.BankApi
import com.bradesco.poc.model.Transaction
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

class FundraiserHistoryActivityPresenter {
    private lateinit var activity: WeakReference<FundraiserHistoryActivity>

    private lateinit var token: String

    fun attach(activity: FundraiserHistoryActivity) {

        activity.intent?.extras?.let {
            token = requireNotNull(it.getString(Extras.ACCESS_TOKEN_EXTRA)) { "missing token" }
        } ?: throw IllegalArgumentException("missing intent extras")

        activity.setLoading(true)
        getUserStatements()

        this.activity = WeakReference(activity)
    }

    private fun runOnUiThread(block: (activity: FundraiserHistoryActivity) -> Unit) {
        activity.get()?.let {
            it.runOnUiThread {
                block(it)
            }
        }
    }

    private val DAY_FORMAT = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    private fun getUserStatements() {
        BankApi.getUserTransactions(token, { transactionResponse ->
            val extracts = transactionResponse?.transactions?.map {
                val date = DAY_FORMAT.parse(it.date ?: "01-01-2018")
                Transaction(date, it.description, it.amount)
            } ?: emptyList()

            runOnUiThread {
                it.setStatements(extracts)
                it.setLoading(false)
            }
        }, {
            setError(it.localizedMessage)
        })
    }

    private fun setError(message: String) {
        activity.get()?.let {
            it.runOnUiThread {
                Toast.makeText(it.applicationContext, message, Toast.LENGTH_LONG).show()
                it.finish()
            }
        }
    }
}
