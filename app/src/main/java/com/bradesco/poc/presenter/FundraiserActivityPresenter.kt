package com.bradesco.poc.presenter

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.widget.Toast
import com.bradesco.poc.activity.Extras
import com.bradesco.poc.activity.FundraiserActivity
import com.bradesco.poc.activity.FundraiserHistoryActivity
import com.bradesco.poc.modules.banksList.ChooseBankActivity;
import com.bradesco.poc.api.BankApi
import com.bradesco.poc.api.model.TransferRequest
import com.bradesco.poc.model.*
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*

private val DAY_FORMAT = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
private const val TRANSACTION_REQUEST = 2
private const val CONFIGURATION_REQUEST = 3
private const val LOGOUT_REQUEST = 4
private const val STATUS_REQUEST = 5

class FundraiserActivityPresenter {
    private lateinit var activity: WeakReference<FundraiserActivity>
    private var fundraiserId = -1

    private lateinit var fundraiser: Fundraiser
    private lateinit var token: String

    private var account = ""
    private var branch = ""

    private var checkingStatus = false

    fun attach(activity: FundraiserActivity) {
        activity.intent?.extras?.let {
            fundraiserId = it.getInt(Extras.FUNDRAISER_ID_EXTRA)
            token = requireNotNull(it.getString(Extras.ACCESS_TOKEN_EXTRA)) { "missing token" }
            account = it.getString(Extras.ACCOUNT_NUMBER_EXTRA, "")
            branch = it.getString(Extras.BRANCH_NUMBER_EXTRA, "")
            val name = it.getString(Extras.USER_ACCOUNT_NAME, "")
            activity.setUser(name, account, branch)
        } ?: throw IllegalArgumentException("missing intent extras")

        fundraiser = fundraisers.firstOrNull { it.id == fundraiserId }
                ?: throw IllegalArgumentException("fundraiser not found")

        fundraiser.apply {
            activity.setFundraiser(name, description, goal, reached)
        }

        activity.setLoading(true)
        getUserBalance()

        this.activity = WeakReference(activity)
    }

    private fun setError(message: String) {
        runOnUiThread {
            Toast.makeText(it.applicationContext, message, Toast.LENGTH_LONG).show()
            it.finish()
        }
    }

    private fun runOnUiThread(block: (activity: FundraiserActivity) -> Unit) {
        activity.get()?.let {
            it.runOnUiThread {
                block(it)
            }
        }
    }

    private fun getUserBalance() {
        BankApi.getUserBalance(token, { balanceResponse ->
            runOnUiThread {
                val balance =  balanceResponse?.balance?.checkingAccount?.value ?: 0f
                it.setBalance(balance, balance, balance)
                getUserTransactions()
            }
        }, {
            setError(it.localizedMessage)
        })
    }

    private fun getUserTransactions() {
        BankApi.getUserTransactions(token, { transactionResponse ->
            val extracts = transactionResponse?.transactions?.map {
                val date = DAY_FORMAT.parse(it.date ?: "01-01-2018")
                Transaction(date, it.description, it.amount)
            } ?: emptyList()

            runOnUiThread {
                it.setTransactionList(extracts)
                it.setLoading(false)
            }
        }, {
            setError(it.localizedMessage)
        })
    }

    fun showPaymentHistory() {
        runOnUiThread {
            val intent = Intent(it.applicationContext, FundraiserHistoryActivity::class.java)
            intent.putExtra(Extras.ACCESS_TOKEN_EXTRA, token)
            it.startActivity(intent)
        }
    }

    private var pendingTransferRequest: TransferRequest? = null
    fun donate(donationAmount: Float) {

        pendingTransferRequest = TransferRequest(
                fundraiser.account.branch,
                fundraiser.account.number,
                fundraiser.name,
                donationAmount)

        runOnUiThread {
            val intent = Intent("com.cognizant.bradesco.modules.transfer").apply {
                putExtra("branch", fundraiser.account.branch)
                putExtra("number", fundraiser.account.number)
                putExtra("amount", donationAmount)
                putExtra("description", fundraiser.name)
                putExtra("app", "Juntaí")
            }

            it.startActivityForResult(intent, TRANSACTION_REQUEST)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, dataIntent: Intent?) {
        when (requestCode) {
            TRANSACTION_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    pendingTransferRequest = null

                    activity.get()?.setPaymentSuccess()
                    getUserBalance()
                } else {
                    toast("Pagamento cancelado!")
                }
            }
            LOGOUT_REQUEST -> chooseNewBank()
            STATUS_REQUEST -> {
                Handler().postDelayed({
                    checkingStatus = false
                }, 2000)
                if (resultCode != Activity.RESULT_OK) {
                    toast("Logout Realizado")
                    chooseNewBank()
                }
            }
        }
    }

    private fun toast(msg: String) {
        activity.get()?.let {
            Toast.makeText(it.applicationContext, msg, Toast.LENGTH_LONG).show()
        }
    }

    fun checkStatus() {
        if (checkingStatus) return
        checkingStatus = true
        val intent = Intent("com.cognizant.bradesco.modules.status").apply {
            putExtra(Extras.BRANCH_NUMBER_EXTRA, branch)
            putExtra(Extras.ACCOUNT_NUMBER_EXTRA, account)
            putExtra("app_packageName", "com.bradesco.poc")
        }
        activity.get()?.startActivityForResult(intent, STATUS_REQUEST)
    }

    private fun chooseNewBank() {
        runOnUiThread {
            val intent = Intent(it, ChooseBankActivity::class.java)
            intent.putExtra(Extras.FUNDRAISER_ID_EXTRA, fundraiserId);
            it.startActivity(intent)
            it.finish()
        }
    }

    fun checkOther() {
        activity.get()?.finish()
    }

    fun onAccountConfigurationClick() {
        val intent = Intent("com.cognizant.bradesco.modules.configuration")
        activity.get()?.startActivityForResult(intent, CONFIGURATION_REQUEST)
    }

    fun onAccountChangeClick() {
        val intent = Intent("com.cognizant.bradesco.modules.logout")
        activity.get()?.startActivityForResult(intent, LOGOUT_REQUEST)
    }
}
