package com.bradesco.poc.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.transition.ChangeBounds
import android.support.transition.Transition
import android.support.transition.TransitionManager
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.FrameLayout
import com.bradesco.poc.R
import com.bradesco.poc.adapter.TransactionListAdapter
import com.bradesco.poc.extensions.formatAsCurrency
import com.bradesco.poc.model.Transaction
import kotlinx.android.synthetic.main.view_balance.view.*
import kotlin.properties.Delegates

class BalanceView(context: Context, attrs: AttributeSet?, defStyle: Int) : FrameLayout(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet?) : this (context, attrs, 0)
    constructor(context: Context) : this (context, null, 0)

    private val view: View
    private val adapter = TransactionListAdapter().also { it.size = 3 }
    private val constraintLayout: ConstraintLayout

    init {
        val inflater = LayoutInflater.from(context)
        view = inflater.inflate(R.layout.view_balance, this, false)
        constraintLayout = view.root_constraint_layout
        addView(view)

        view.label_expand_balance.setOnClickListener {
            expanded = !expanded
        }

        short_history_recycler.adapter = adapter
        short_history_recycler.layoutManager = LinearLayoutManager(context)

        view.full_history_btn.setOnClickListener {
            onPaymentHistoryClick?.invoke()
        }
    }

    var transactions: List<Transaction> by Delegates.observable(listOf()) { _, _, value ->
        adapter.transactions = value
    }

    var balance: Float by Delegates.observable(0f) { _, _, value ->
        view.balance.text = value.formatAsCurrency()
    }

    var availableBalance: Float by Delegates.observable(0f) { _, _, value ->
        view.available_balance.text = value.formatAsCurrency()
    }

    var checkingBalance: Float by Delegates.observable(0f) { _, _, value ->
        view.checking_balance.text = value.formatAsCurrency()
    }

    var expanded: Boolean by Delegates.observable(false) { _, _, value ->
        view.label_expand_balance.text = if (value) {
            "ocultar detalhes"
        } else {
            "ver mais detalhes"
        }
        expandView(value)
    }

    var displayMonth: Boolean by Delegates.observable(true) { _, _, value ->
        adapter.displayMonth = value
    }

    private fun expandView(expand: Boolean) {
        val changeBounds = ChangeBounds()
        changeBounds.interpolator = AccelerateInterpolator()
        changeBounds.duration = 100
        changeBounds.addListener(object: Transition.TransitionListener {
            override fun onTransitionResume(transition: Transition) = Unit
            override fun onTransitionPause(transition: Transition) = Unit
            override fun onTransitionCancel(transition: Transition) = Unit
            override fun onTransitionStart(transition: Transition) {
                view.expandable_view.alpha = 0f
            }
            override fun onTransitionEnd(transition: Transition) {
                view.expandable_view.alpha = 1f
            }
        })

        TransitionManager.beginDelayedTransition(constraintLayout, changeBounds)
        val height = if (!expand) 1 else ConstraintLayout.LayoutParams.WRAP_CONTENT

        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.constrainHeight(R.id.expandable_view, height)
        constraintSet.applyTo(constraintLayout)
    }

    var onPaymentHistoryClick: (() -> Unit)? = null
}
