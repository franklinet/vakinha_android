package com.bradesco.poc.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.PopupMenu
import com.bradesco.poc.R
import kotlinx.android.synthetic.main.view_header.view.*
import kotlin.properties.Delegates

class HeaderView(context: Context, attrs: AttributeSet?, defStyle: Int) : FrameLayout(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet?) : this (context, attrs, 0)
    constructor(context: Context) : this (context, null, 0)

    private val view: View

    init {
        val inflater = LayoutInflater.from(context)
        view = inflater.inflate(R.layout.view_header, this, false)
        addView(view)
        view.my_account_options_btn.setOnClickListener {
            PopupMenu(context, view.my_account_options_btn).apply {
                setOnMenuItemClickListener(optionListener)
                inflate(R.menu.menu_my_account)
                show()
            }
        }
        view.change_account_btn.setOnClickListener {
            onAccountChangeClick?.invoke()
        }
    }

    val optionListener = object : PopupMenu.OnMenuItemClickListener {
        override fun onMenuItemClick(menuItem: MenuItem): Boolean {
            onConfigurationClick?.invoke()
            return true
        }
    }

    var onConfigurationClick: (() -> Unit)? = null
    var onAccountChangeClick: (() -> Unit)? = null

    var userName: String by Delegates.observable("") { _, _, value ->
        view.header_user_name.text = value
    }

    var branch: String by Delegates.observable("") { _, _, value ->
        view.header_branch.text = "Ag $value"
    }

    var account: String by Delegates.observable("") { _, _, value ->
        view.header_account.text = "C/C $value"
    }
}
