package com.bradesco.poc.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.RadioButton
import android.widget.RadioGroup
import com.bradesco.poc.R
import kotlin.math.round

class ConnectedRadioGroup(context: Context, attrs: AttributeSet?) : RadioGroup(context, attrs) {
    constructor(context: Context) : this(context, null)

    private val accentColor = ContextCompat.getColor(context, R.color.cognizantBlue)
    private val notSelectedColor = ContextCompat.getColor(context, R.color.cognizantGray)
    private val paint = Paint().also {
        it.color = accentColor
        it.strokeWidth = 3 * context.resources.displayMetrics.density
        it.style = Paint.Style.STROKE
    }

    init {
        setWillNotDraw(false)

        super.setOnCheckedChangeListener { group, checkedId ->
            listener?.onCheckedChanged(group, checkedId)

            for (i in 0 until childCount) {
                val child = getChildAt(i)
                if (child is RadioButton) {
                    if (child.id == checkedId) {
                        child.textSize = 14f
                        child.scaleX = 1.2f
                        child.scaleY = 1.2f
                        child.setTextColor(accentColor)
                    } else {
                        child.textSize = 12f
                        child.scaleX = 1f
                        child.scaleY = 1f
                        child.setTextColor(notSelectedColor)
                    }
                }
            }
            invalidate()
        }
    }

    private fun checkedView(): RadioButton {
        return findViewById<RadioButton>(checkedRadioButtonId)
    }

    private fun anyUncheckedView(): RadioButton {
        return (0 until childCount).asSequence()
                .map { getChildAt(it) }
                .first { it.id != checkedRadioButtonId } as RadioButton
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        val checkedView = checkedView()
        val anyUncheckedView = anyUncheckedView()

        val offset = (checkedView.height - anyUncheckedView.height)

        checkedView.layout(checkedView.left,
                           checkedView.top - offset,
                           checkedView.right,
                           checkedView.bottom - offset)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        setMeasuredDimension(measuredWidth, round(measuredHeight * 1.4f).toInt())
    }

    override fun onDraw(canvas: Canvas) {

        val firstChild = getChildAt(0)
        val lastChild = getChildAt(childCount - 1)

        val baselineView = anyUncheckedView()

        val y = baselineView.y + round(0.85*(baselineView.height)).toFloat()

        canvas.drawLine(
                firstChild.x + firstChild.width/2,
                y,
                lastChild.x + lastChild.width/2,
                y,
                paint)
    }

    private var listener: OnCheckedChangeListener? = null
    override fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        this.listener = listener
    }
}
