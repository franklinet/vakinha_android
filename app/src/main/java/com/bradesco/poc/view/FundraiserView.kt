package com.bradesco.poc.view

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.bradesco.poc.R
import com.bradesco.poc.extensions.formatAsCurrency
import kotlinx.android.synthetic.main.view_fundraiser.view.*
import kotlin.properties.Delegates

class FundraiserView(context: Context, attrs: AttributeSet?, defStyle: Int) : FrameLayout(context, attrs, defStyle) {
    constructor(context: Context, attrs: AttributeSet?) : this (context, attrs, 0)
    constructor(context: Context) : this (context, null, 0)

    private val view: View
    init {
        val inflater = LayoutInflater.from(context)
        view = inflater.inflate(R.layout.view_fundraiser, this, false)
        addView(view)

        view.amount_radio_group.setOnCheckedChangeListener { _, checkedId ->
            checkedDonationAmount = when (checkedId) {
                R.id.rb15 -> 15f
                R.id.rb20 -> 20f
                R.id.rb30 -> 30f
                R.id.rb40 -> 40f
                R.id.rb50 -> 50f
                R.id.rb100 -> 100f
                else -> 0f
            }
        }

        view.participate_btn.setOnClickListener {
            /*view.layout_donate.visibility = View.GONE*/
            /*view.layout_donate_success.visibility = View.GONE*/
            /*view.layout_loading.visibility = View.VISIBLE*/
            onDonationClicked?.invoke(checkedDonationAmount)
        }

        view.choose_other_btn.setOnClickListener { onCheckOthersClicked?.invoke() }
        view.success_choose_another_btn.setOnClickListener { onCheckOthersClicked?.invoke() }
        view.payment_history_btn.setOnClickListener { onPaymentHistoryClick?.invoke() }
    }

    fun setPaymentSuccess() {
        view.layout_donate.visibility = View.GONE
        view.layout_donate_success.visibility = View.VISIBLE
        view.layout_loading.visibility = View.GONE
    }

    private var checkedDonationAmount: Float = 15f

    var name: String by Delegates.observable("") { _, _, value ->
        view.fundraiser_name.text = value
        val text = SpannableStringBuilder().apply {
            append("Sua transferência para participar no ")
            val start = length
            append(value)
            setSpan(StyleSpan(android.graphics.Typeface.BOLD),
                    start, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            append(" foi efetivada!")
        }
        view.success_transfer_text.text = text
    }

    var description: String by Delegates.observable("") { _, _, value ->
        view.fundraiser_description.text = value
    }

    var goal: Float by Delegates.observable(0f) { _, _, value ->
        view.fundraiser_goal.text = value.formatAsCurrency()
        updatedReachedOrGoal()
    }

    var reached: Float by Delegates.observable(0f) { _, _, value ->
        view.fundraiser_reached.text = value.formatAsCurrency()
        updatedReachedOrGoal()
    }

    private fun updatedReachedOrGoal() {
        if (reached >= goal) {
            view.fundraiser_reached.setTextColor(
                    ContextCompat.getColor(view.context, R.color.cognizantGreen)
            )
        } else {
            view.fundraiser_reached.setTextColor(Color.RED)
        }
    }

    var onDonationClicked: ((Float) -> Unit)? = null
    var onCheckOthersClicked: (() -> Unit)? = null
    var onPaymentHistoryClick: (() -> Unit)? = null
}
