package com.bradesco.poc;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bradesco.poc.modules.vaquinhalist.VaquinhaListActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startVaquinhaListActovity();
            }
        }, 3 * 1000);
    }

    private void startVaquinhaListActovity() {
        Intent vaquinhaList = new Intent(this, VaquinhaListActivity.class);
        startActivity(vaquinhaList);
        finish();
    }
}
