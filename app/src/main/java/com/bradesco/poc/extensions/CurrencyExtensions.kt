package com.bradesco.poc.extensions

import java.lang.Math.abs

fun Float.formatAsCurrency(): String {
    return buildString {
        if (this@formatAsCurrency < 0) append("- ")
        append(String.format("R$ %.2f", abs(this@formatAsCurrency)))
    }
}
