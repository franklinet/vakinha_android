
# UltimosLancamentosExtratoContaCorrenteResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**contaPoupancaFacil** | **Long** | Conta Poupan�a F�cil (ou Linkada) |  [optional]
**lancamentos** | [**List&lt;OcorrenciaUltimosLancamentosExtrato&gt;**](OcorrenciaUltimosLancamentosExtrato.md) | Ultimos Lan�amentos |  [optional]
**lancamentosIdentificados** | [**List&lt;OcorrenciaUltimosLancamentosIdentificacaoExtrato&gt;**](OcorrenciaUltimosLancamentosIdentificacaoExtrato.md) | Ultimos lan�amentos Identificados |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero da Conta |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**statusCoberturaAutomatica** | **String** | Status da cobertura autom�tica |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente |  [optional]
**statusContaPoupanca** | **String** | Status da conta poupan�a |  [optional]



