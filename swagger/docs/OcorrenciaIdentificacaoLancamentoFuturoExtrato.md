
# OcorrenciaIdentificacaoLancamentoFuturoExtrato

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoLancamento** | **Long** | C�digo do lan�amento. |  [optional]
**dataLancamento** | **String** | Data do lan�amento. (format yyyy-MM-dd) |  [optional]
**descricaoCompletaLancamento** | **String** | Descri��o Completa do lan�amento. |  [optional]
**descricaoResumidaLancamento** | **String** | Descri��o Resumida do lan�amento. |  [optional]
**identificacaoLancamento** | **String** | Identifica��o do lan�amento. |  [optional]
**numeroDocumento** | **Long** | N�mero do documento. |  [optional]
**tipoLancamento** | **String** | Tipo do lan�amento. |  [optional]
**valorLancamento** | **Double** | Valor do Lan�amento. |  [optional]



