# BankStatementControllerApi

All URIs are relative to *https://10.195.182.53:8380/api-front-entr-1.0.6*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExtratoDemonstativoContaPoupancaUsingGET**](BankStatementControllerApi.md#getExtratoDemonstativoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/demonstrativos | Extrato Demonstrativo - Conta Poupan�a.
[**getExtratoMensalContaCorrenteUsingGET**](BankStatementControllerApi.md#getExtratoMensalContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/mensal | Extrato Mensal de uma Conta Corrente.
[**getExtratoMensalContaPoupancaUsingGET**](BankStatementControllerApi.md#getExtratoMensalContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/mensal | Extrato Mensal de uma Conta Poupanca.
[**getExtratoPeriodoContaCorrenteUsingGET**](BankStatementControllerApi.md#getExtratoPeriodoContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/periodo | Extrato Por periodo de uma Conta Corrente.
[**getExtratoPeriodoContaPoupancaUsingGET**](BankStatementControllerApi.md#getExtratoPeriodoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/periodo | Extrato Por periodo de uma Conta Poupanca.
[**getExtratosLancamentosFuturosContaCorrenteUsingGET**](BankStatementControllerApi.md#getExtratosLancamentosFuturosContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/lancamentosFuturos | Extrato lan�amentos futuros de uma Conta Corrente.
[**getExtratosLancamentosFuturosContaPoupancaUsingGET**](BankStatementControllerApi.md#getExtratosLancamentosFuturosContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/lancamentosFuturos | Extrato lan�amentos futuros de uma Conta Poupan�a.
[**getSaldoContaCorrenteUsingGET**](BankStatementControllerApi.md#getSaldoContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/saldos | Consulta o Saldo de uma Conta Corrente.
[**getSaldoContaPoupancaUsingGET**](BankStatementControllerApi.md#getSaldoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/saldos | Consulta o Saldo de uma Conta Poupan�a.
[**getUltimosLancamentosContaCorrenteUsingGET**](BankStatementControllerApi.md#getUltimosLancamentosContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/ultimosLancamentos | Extrato Ultimos Lan�amentos - Conta Corrente.
[**getUltimosLancamentosContaPoupancaUsingGET**](BankStatementControllerApi.md#getUltimosLancamentosContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/ultimosLancamentos | Extrato Ultimos Lan�amentos - Conta Poupan�a.


<a name="getExtratoDemonstativoContaPoupancaUsingGET"></a>
# **getExtratoDemonstativoContaPoupancaUsingGET**
> ExtratoDemonstativoContaPoupancaResponse getExtratoDemonstativoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Extrato Demonstrativo - Conta Poupan�a.

Extrato Demonstrativo - Conta Poupan�a.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    ExtratoDemonstativoContaPoupancaResponse result = apiInstance.getExtratoDemonstativoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratoDemonstativoContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**ExtratoDemonstativoContaPoupancaResponse**](ExtratoDemonstativoContaPoupancaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratoMensalContaCorrenteUsingGET"></a>
# **getExtratoMensalContaCorrenteUsingGET**
> ExtratoMensalResponse getExtratoMensalContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, mes, accessToken)

Extrato Mensal de uma Conta Corrente.

Extrato Mensal de uma Conta Corrente.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
Integer mes = 56; // Integer | Mes para Consultar o Extrato (1-Janeiro ... 12-Dezembro)
String accessToken = "accessToken_example"; // String | API access token
try {
    ExtratoMensalResponse result = apiInstance.getExtratoMensalContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, mes, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratoMensalContaCorrenteUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **mes** | **Integer**| Mes para Consultar o Extrato (1-Janeiro ... 12-Dezembro) |
 **accessToken** | **String**| API access token |

### Return type

[**ExtratoMensalResponse**](ExtratoMensalResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratoMensalContaPoupancaUsingGET"></a>
# **getExtratoMensalContaPoupancaUsingGET**
> ExtratoMensalResponse getExtratoMensalContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, mes, accessToken)

Extrato Mensal de uma Conta Poupanca.

Extrato Mensal de uma Conta Poupanca.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
Integer mes = 56; // Integer | Mes para Consultar o Extrato (1-Janeiro  ... 12-Dezembro)
String accessToken = "accessToken_example"; // String | API access token
try {
    ExtratoMensalResponse result = apiInstance.getExtratoMensalContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, mes, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratoMensalContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **mes** | **Integer**| Mes para Consultar o Extrato (1-Janeiro  ... 12-Dezembro) |
 **accessToken** | **String**| API access token |

### Return type

[**ExtratoMensalResponse**](ExtratoMensalResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratoPeriodoContaCorrenteUsingGET"></a>
# **getExtratoPeriodoContaCorrenteUsingGET**
> ExtratoPeriodoResponse getExtratoPeriodoContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, dataInicial, dataFinal, accessToken)

Extrato Por periodo de uma Conta Corrente.

Extrato por periodo de uma Conta Corrente.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String dataInicial = "dataInicial_example"; // String | Data inicial - (format yyyy-MM-dd).
String dataFinal = "dataFinal_example"; // String | Data Final -(format yyyy-MM-dd).
String accessToken = "accessToken_example"; // String | API access token
try {
    ExtratoPeriodoResponse result = apiInstance.getExtratoPeriodoContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, dataInicial, dataFinal, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratoPeriodoContaCorrenteUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **dataInicial** | **String**| Data inicial - (format yyyy-MM-dd). |
 **dataFinal** | **String**| Data Final -(format yyyy-MM-dd). |
 **accessToken** | **String**| API access token |

### Return type

[**ExtratoPeriodoResponse**](ExtratoPeriodoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratoPeriodoContaPoupancaUsingGET"></a>
# **getExtratoPeriodoContaPoupancaUsingGET**
> ExtratoPeriodoResponse getExtratoPeriodoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, dataInicial, dataFinal, accessToken)

Extrato Por periodo de uma Conta Poupanca.

Extrato por periodo de uma Conta Poupanca.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String dataInicial = "dataInicial_example"; // String | Data inicial - (format yyyy-MM-dd).
String dataFinal = "dataFinal_example"; // String | Data Final -(format yyyy-MM-dd).
String accessToken = "accessToken_example"; // String | API access token
try {
    ExtratoPeriodoResponse result = apiInstance.getExtratoPeriodoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, dataInicial, dataFinal, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratoPeriodoContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **dataInicial** | **String**| Data inicial - (format yyyy-MM-dd). |
 **dataFinal** | **String**| Data Final -(format yyyy-MM-dd). |
 **accessToken** | **String**| API access token |

### Return type

[**ExtratoPeriodoResponse**](ExtratoPeriodoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratosLancamentosFuturosContaCorrenteUsingGET"></a>
# **getExtratosLancamentosFuturosContaCorrenteUsingGET**
> LancamentosFuturosExtratoContaCorrenteResponse getExtratosLancamentosFuturosContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Extrato lan�amentos futuros de uma Conta Corrente.

Extrato lan�amentos futuros de uma Conta Corrente.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    LancamentosFuturosExtratoContaCorrenteResponse result = apiInstance.getExtratosLancamentosFuturosContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratosLancamentosFuturosContaCorrenteUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**LancamentosFuturosExtratoContaCorrenteResponse**](LancamentosFuturosExtratoContaCorrenteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getExtratosLancamentosFuturosContaPoupancaUsingGET"></a>
# **getExtratosLancamentosFuturosContaPoupancaUsingGET**
> LancamentosFuturosExtratoContaPoupancaResponse getExtratosLancamentosFuturosContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Extrato lan�amentos futuros de uma Conta Poupan�a.

Extrato lan�amentos futuros de uma Conta Poupan�a.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    LancamentosFuturosExtratoContaPoupancaResponse result = apiInstance.getExtratosLancamentosFuturosContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getExtratosLancamentosFuturosContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**LancamentosFuturosExtratoContaPoupancaResponse**](LancamentosFuturosExtratoContaPoupancaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSaldoContaCorrenteUsingGET"></a>
# **getSaldoContaCorrenteUsingGET**
> SaldoContaCorrenteResponse getSaldoContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Consulta o Saldo de uma Conta Corrente.

Consulta o Saldo de uma Conta Corrente.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    SaldoContaCorrenteResponse result = apiInstance.getSaldoContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getSaldoContaCorrenteUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**SaldoContaCorrenteResponse**](SaldoContaCorrenteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSaldoContaPoupancaUsingGET"></a>
# **getSaldoContaPoupancaUsingGET**
> SaldoContaPoupancaResponse getSaldoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Consulta o Saldo de uma Conta Poupan�a.

Consulta o Saldo de uma Conta Poupan�a

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    SaldoContaPoupancaResponse result = apiInstance.getSaldoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getSaldoContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**SaldoContaPoupancaResponse**](SaldoContaPoupancaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUltimosLancamentosContaCorrenteUsingGET"></a>
# **getUltimosLancamentosContaCorrenteUsingGET**
> UltimosLancamentosExtratoContaCorrenteResponse getUltimosLancamentosContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Extrato Ultimos Lan�amentos - Conta Corrente.

Extrato Ultimos Lan�amentos - Conta Corrente.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    UltimosLancamentosExtratoContaCorrenteResponse result = apiInstance.getUltimosLancamentosContaCorrenteUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getUltimosLancamentosContaCorrenteUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**UltimosLancamentosExtratoContaCorrenteResponse**](UltimosLancamentosExtratoContaCorrenteResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUltimosLancamentosContaPoupancaUsingGET"></a>
# **getUltimosLancamentosContaPoupancaUsingGET**
> UltimosLancamentosExtratoContaPoupancaResponse getUltimosLancamentosContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken)

Extrato Ultimos Lan�amentos - Conta Poupan�a.

Extrato Ultimos Lan�amentos - Conta Poupan�a.

### Example
```java
// Import classes:
//import com.bradesco.swagger.ApiException;
//import com.bradesco.swagger.api.BankStatementControllerApi;


BankStatementControllerApi apiInstance = new BankStatementControllerApi();
String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
Integer agencia = 56; // Integer | Ag�ncia da Conta
Long numeroConta = 789L; // Long | N�mero da Conta
Integer razaoConta = 56; // Integer | Raz�o da conta
String accessToken = "accessToken_example"; // String | API access token
try {
    UltimosLancamentosExtratoContaPoupancaResponse result = apiInstance.getUltimosLancamentosContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BankStatementControllerApi#getUltimosLancamentosContaPoupancaUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codigoPeriferico** | **String**| C�digo do Perif�rico |
 **agencia** | **Integer**| Ag�ncia da Conta |
 **numeroConta** | **Long**| N�mero da Conta |
 **razaoConta** | **Integer**| Raz�o da conta |
 **accessToken** | **String**| API access token |

### Return type

[**UltimosLancamentosExtratoContaPoupancaResponse**](UltimosLancamentosExtratoContaPoupancaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

