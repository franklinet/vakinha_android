
# OcorrenciasSaldosDiversos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descricao** | **String** | Descri��o do Produto. |  [optional]
**identificacaoBaixaAutomatica** | **String** |    A, B, C, D ou E � Montar o pr�prio conte�do recebido   conforme lay-out de impress�o&lt;br /&gt;   1 � Montar a literal A+B&lt;br /&gt;   2 � Montar a literal A+B+C&lt;br /&gt;  3 � Montar a literal A+B+C+D&lt;br /&gt;   4 � Montar a literal �A+B+C+D+E�&lt;br /&gt;   � � - Linhas sem literal&lt;br /&gt;  |  [optional]
**identificacaoProduto** | **Long** | C�digo de Identifica��o do produto. |  [optional]
**identificacaoSaldo** | **String** | Identificador do saldo. &lt;br /&gt; 0 - Diversos, saldo n�o tem complemento no nome. &lt;br/&gt;1 - Saldo livre/car�ncia com baixa autom�tica, montar complemento &#39;C/BAIXA AUTOMATICA&#39; &lt;br /&gt;2 - Saldo livre sem baixa autom�tica, montar complemento &#39;P/RESGATE&#39; &lt;br /&gt;3 - Saldo em car�ncia sem baixa autom�tica, montar complemento &#39;CARENCIA&#39; &lt;br /&gt;9 - Novos produtos, sem complemento.  |  [optional]
**saldo** | **Double** | Saldo. |  [optional]



