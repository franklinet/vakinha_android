
# BusinessError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**mensagemRetorno** | **String** | Mensagem de Retorno. |  [optional]



