
# OcorrenciaIdentificacaoExtrato

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoLancamento** | **Long** | C�digo do Lan�amento (historico). |  [optional]
**dataDebitoCpmf** | **String** | Data do debito do CPMF. (format: yyyy-MM-dd) |  [optional]
**dataLancamento** | **String** | Data do lan�amento. (format: yyyy-MM-dd) |  [optional]
**descricaoCompletaLancamento** | **String** | Descri��o Completa do lan�amento. |  [optional]
**descricaoResumidaLancamento** | **String** | Descri��o Resumida do lan�amento. |  [optional]
**identificacaoLancamento** | **String** | Identifica��o do lan�amento. |  [optional]
**numeroDocumento** | **Long** | Numero do documento. |  [optional]
**tipoLancamento** | **String** | Tipo do lan�amento. |  [optional]
**valorCpmf** | **Double** | Valor do CPMF. |  [optional]
**valorLancamento** | **Double** | Valor do lan�amento. |  [optional]



