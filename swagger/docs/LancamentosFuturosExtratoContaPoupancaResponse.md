
# LancamentosFuturosExtratoContaPoupancaResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | Codigo de Retorno. |  [optional]
**identificadorModalidadeConta** | **String** | Identificador da modalidade da conta&lt;br /&gt; 1 - Poupan�a Tradicional &lt;br /&gt;2 - Poupan�a F�cil |  [optional]
**lancamentos** | [**List&lt;OcorrenciaLancamentoFuturoExtrato&gt;**](OcorrenciaLancamentoFuturoExtrato.md) | Lan�amentos Futuros |  [optional]
**lancamentosIdentificados** | [**List&lt;OcorrenciaIdentificacaoLancamentoFuturoExtrato&gt;**](OcorrenciaIdentificacaoLancamentoFuturoExtrato.md) | Lan�amentos Futuros Identificados. |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero  da Conta |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente |  [optional]
**statusContaPoupanca** | **String** | Status da conta poupan�a |  [optional]



