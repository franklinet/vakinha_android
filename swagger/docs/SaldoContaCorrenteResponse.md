
# SaldoContaCorrenteResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | Codigo da Mensagem. |  [optional]
**contaPoupancaFacil** | **Long** | Conta Poupan�a F�cil (ou Linkada). |  [optional]
**creditoPreAprovado** | [**OcorrenciaCreditoPreAprovado**](OcorrenciaCreditoPreAprovado.md) | Data Limite do Cr�dito pr�-aprovado. |  [optional]
**nomeCliente** | **String** | Nome do Cliente. |  [optional]
**numeroConta** | **Long** | Conta do cliente. |  [optional]
**razaoConta** | **Integer** | Raz�o pesquisada. |  [optional]
**saldosDiversos** | [**List&lt;OcorrenciasSaldosDiversos&gt;**](OcorrenciasSaldosDiversos.md) | Saldos Diversos |  [optional]
**statusCoberturaAutomatica** | **String** | Status da cobertura Autom�tica. |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente. |  [optional]
**statusContaPoupanca** | **String** | Status da conta Poupan�a. |  [optional]



