
# ExtratoMensalResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**nomeCliente** | **String** | Nome do cliente. |  [optional]
**numeroConta** | **Long** | Conta a mostrar no extrato. |  [optional]
**ocorrenciaExtratos** | [**List&lt;OcorrenciaExtrato&gt;**](OcorrenciaExtrato.md) | Ocorrencias do Extrato. |  [optional]
**ocorrenciaIdentificacaoExtratos** | [**List&lt;OcorrenciaIdentificacaoExtrato&gt;**](OcorrenciaIdentificacaoExtrato.md) | Ocorrencias de Identifica��o do Extrato. |  [optional]
**razaoConta** | **Integer** | Raz�o da conta. |  [optional]



