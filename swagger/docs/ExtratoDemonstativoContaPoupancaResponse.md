
# ExtratoDemonstativoContaPoupancaResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**dataCorrenteSaldo** | **String** | Data Corrente do Saldo. (format: yyyy-MM-dd) |  [optional]
**identificadorModalidadeConta** | **String** | Identificador da modalidade da conta&lt;br /&gt; 1 - Poupan�a tradicional &lt;br /&gt;2? - Poupan�a F�cil |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero da Conta |  [optional]
**ocorrenciasExtratosDemonstrativos** | [**List&lt;OcorrenciaExtratoDemonstrativo&gt;**](OcorrenciaExtratoDemonstrativo.md) | Ocorrencias de Saldo do Extrato demonstrativo. |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente |  [optional]
**statusContaPoupanca** | **String** | Status da conta poupan�a |  [optional]
**totalRendimentos** | **Double** | Total de Rendimentos. |  [optional]
**totalSaldo** | **Double** | Total do Saldo. |  [optional]



