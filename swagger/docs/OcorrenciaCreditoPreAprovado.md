
# OcorrenciaCreditoPreAprovado

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataLimite** | **String** | Data limite cr�dito pr�-aprovado. (format yyyy-MM-dd) |  [optional]
**descricao** | **String** | Descri��o |  [optional]
**identificacaoBaixaAutomatica** | **String** |    A, B, C, D ou E � Montar o pr�prio conte�do recebido   conforme lay-out de impress�o&lt;br /&gt;   1 � Montar a literal A+B&lt;br /&gt;   2 � Montar a literal A+B+C&lt;br /&gt;  3 � Montar a literal A+B+C+D&lt;br /&gt;   4 � Montar a literal �A+B+C+D+E�&lt;br /&gt;   � � - Linhas sem literal&lt;br /&gt;  |  [optional]



