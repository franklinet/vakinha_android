
# SaldoContaPoupancaResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | Codigo de Retorno. |  [optional]
**identificadorModalidadeConta** | **String** | Identificador da modalidade da conta&lt;br /&gt; 1 - Poupan�a tradicional &lt;br /&gt;2- Poupan�a F�cil |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero da Conta |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**saldosDiversos** | [**List&lt;OcorrenciasSaldosDiversos&gt;**](OcorrenciasSaldosDiversos.md) | Saldos Diversos |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente. |  [optional]
**statusContaPoupanca** | **String** | Status da conta Poupan�a. |  [optional]



