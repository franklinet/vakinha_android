
# OcorrenciaExtratoDemonstrativo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diaAniversario** | **String** | Dia do anivers�rio. |  [optional]
**identificadorSaldo** | **String** | Identificador do Saldo. |  [optional]
**indicePoupanca** | **Double** | �ndice de poupan�a. |  [optional]
**rendimento** | **Double** | Rendimentos. |  [optional]
**saldoAniversario** | **Double** | Saldo do anivers�rio. |  [optional]
**saldoBase** | **Double** | Saldo base para calculo. |  [optional]



