
# UltimosLancamentosExtratoContaPoupancaResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**identificadorModalidadeConta** | **String** | Identificador da modalidade da conta&lt;br /&gt; 1� - Poupan�a tradicional &lt;br /&gt;�2� - Poupan�a f�cil |  [optional]
**lancamentos** | [**List&lt;OcorrenciaUltimosLancamentosExtrato&gt;**](OcorrenciaUltimosLancamentosExtrato.md) | Ultimos Lan�amentos |  [optional]
**lancamentosIdentificados** | [**List&lt;OcorrenciaUltimosLancamentosIdentificacaoExtrato&gt;**](OcorrenciaUltimosLancamentosIdentificacaoExtrato.md) | Ultimos lan�amentos Identificados |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero da Conta |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente |  [optional]
**statusContaPoupanca** | **String** | Status da conta poupan�a |  [optional]



