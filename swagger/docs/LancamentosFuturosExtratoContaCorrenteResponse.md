
# LancamentosFuturosExtratoContaCorrenteResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**codigoRetorno** | **Integer** | C�digo de Retorno. |  [optional]
**contaPoupancaFacil** | **Long** | Conta Poupan�a Facil (ou Linkada) |  [optional]
**lancamentos** | [**List&lt;OcorrenciaLancamentoFuturoExtrato&gt;**](OcorrenciaLancamentoFuturoExtrato.md) | Lan�amentos Futuros |  [optional]
**lancamentosIdentificados** | [**List&lt;OcorrenciaIdentificacaoLancamentoFuturoExtrato&gt;**](OcorrenciaIdentificacaoLancamentoFuturoExtrato.md) | Lan�amentos Futuros Identificados. |  [optional]
**nomeCliente** | **String** | Nome do Cliente |  [optional]
**numeroConta** | **Long** | N�mero da Conta |  [optional]
**razaoConta** | **Integer** | Raz�o da Conta |  [optional]
**statusCoberturaAutomatica** | **String** | Status da cobertura autom�tica |  [optional]
**statusContaCorrente** | **String** | Status da conta corrente |  [optional]
**statusContaPoupanca** | **String** | Status da conta poupan�a. |  [optional]



