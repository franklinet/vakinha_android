# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.bradesco.swagger.*;
import com.bradesco.swagger.auth.*;
import com.bradesco.swagger.model.*;
import com.bradesco.swagger.api.BankStatementControllerApi;

import java.io.File;
import java.util.*;

public class BankStatementControllerApiExample {

    public static void main(String[] args) {
        
        BankStatementControllerApi apiInstance = new BankStatementControllerApi();
        String codigoPeriferico = "codigoPeriferico_example"; // String | C�digo do Perif�rico
        Integer agencia = 56; // Integer | Ag�ncia da Conta
        Long numeroConta = 789L; // Long | N�mero da Conta
        Integer razaoConta = 56; // Integer | Raz�o da conta
        String accessToken = "accessToken_example"; // String | API access token
        try {
            ExtratoDemonstativoContaPoupancaResponse result = apiInstance.getExtratoDemonstativoContaPoupancaUsingGET(codigoPeriferico, agencia, numeroConta, razaoConta, accessToken);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BankStatementControllerApi#getExtratoDemonstativoContaPoupancaUsingGET");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://10.195.182.53:8380/api-front-entr-1.0.6*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*BankStatementControllerApi* | [**getExtratoDemonstativoContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getExtratoDemonstativoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/demonstrativos | Extrato Demonstrativo - Conta Poupan�a.
*BankStatementControllerApi* | [**getExtratoMensalContaCorrenteUsingGET**](docs/BankStatementControllerApi.md#getExtratoMensalContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/mensal | Extrato Mensal de uma Conta Corrente.
*BankStatementControllerApi* | [**getExtratoMensalContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getExtratoMensalContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/mensal | Extrato Mensal de uma Conta Poupanca.
*BankStatementControllerApi* | [**getExtratoPeriodoContaCorrenteUsingGET**](docs/BankStatementControllerApi.md#getExtratoPeriodoContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/periodo | Extrato Por periodo de uma Conta Corrente.
*BankStatementControllerApi* | [**getExtratoPeriodoContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getExtratoPeriodoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/periodo | Extrato Por periodo de uma Conta Poupanca.
*BankStatementControllerApi* | [**getExtratosLancamentosFuturosContaCorrenteUsingGET**](docs/BankStatementControllerApi.md#getExtratosLancamentosFuturosContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/lancamentosFuturos | Extrato lan�amentos futuros de uma Conta Corrente.
*BankStatementControllerApi* | [**getExtratosLancamentosFuturosContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getExtratosLancamentosFuturosContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/lancamentosFuturos | Extrato lan�amentos futuros de uma Conta Poupan�a.
*BankStatementControllerApi* | [**getSaldoContaCorrenteUsingGET**](docs/BankStatementControllerApi.md#getSaldoContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/saldos | Consulta o Saldo de uma Conta Corrente.
*BankStatementControllerApi* | [**getSaldoContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getSaldoContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/saldos | Consulta o Saldo de uma Conta Poupan�a.
*BankStatementControllerApi* | [**getUltimosLancamentosContaCorrenteUsingGET**](docs/BankStatementControllerApi.md#getUltimosLancamentosContaCorrenteUsingGET) | **GET** /consultas/contaCorrente/extratos/ultimosLancamentos | Extrato Ultimos Lan�amentos - Conta Corrente.
*BankStatementControllerApi* | [**getUltimosLancamentosContaPoupancaUsingGET**](docs/BankStatementControllerApi.md#getUltimosLancamentosContaPoupancaUsingGET) | **GET** /consultas/contaPoupanca/extratos/ultimosLancamentos | Extrato Ultimos Lan�amentos - Conta Poupan�a.


## Documentation for Models

 - [BusinessError](docs/BusinessError.md)
 - [ExtratoDemonstativoContaPoupancaResponse](docs/ExtratoDemonstativoContaPoupancaResponse.md)
 - [ExtratoMensalResponse](docs/ExtratoMensalResponse.md)
 - [ExtratoPeriodoResponse](docs/ExtratoPeriodoResponse.md)
 - [LancamentosFuturosExtratoContaCorrenteResponse](docs/LancamentosFuturosExtratoContaCorrenteResponse.md)
 - [LancamentosFuturosExtratoContaPoupancaResponse](docs/LancamentosFuturosExtratoContaPoupancaResponse.md)
 - [OcorrenciaCreditoPreAprovado](docs/OcorrenciaCreditoPreAprovado.md)
 - [OcorrenciaExtrato](docs/OcorrenciaExtrato.md)
 - [OcorrenciaExtratoDemonstrativo](docs/OcorrenciaExtratoDemonstrativo.md)
 - [OcorrenciaIdentificacaoExtrato](docs/OcorrenciaIdentificacaoExtrato.md)
 - [OcorrenciaIdentificacaoLancamentoFuturoExtrato](docs/OcorrenciaIdentificacaoLancamentoFuturoExtrato.md)
 - [OcorrenciaLancamentoFuturoExtrato](docs/OcorrenciaLancamentoFuturoExtrato.md)
 - [OcorrenciaUltimosLancamentosExtrato](docs/OcorrenciaUltimosLancamentosExtrato.md)
 - [OcorrenciaUltimosLancamentosIdentificacaoExtrato](docs/OcorrenciaUltimosLancamentosIdentificacaoExtrato.md)
 - [OcorrenciasSaldosDiversos](docs/OcorrenciasSaldosDiversos.md)
 - [SaldoContaCorrenteResponse](docs/SaldoContaCorrenteResponse.md)
 - [SaldoContaPoupancaResponse](docs/SaldoContaPoupancaResponse.md)
 - [UltimosLancamentosExtratoContaCorrenteResponse](docs/UltimosLancamentosExtratoContaCorrenteResponse.md)
 - [UltimosLancamentosExtratoContaPoupancaResponse](docs/UltimosLancamentosExtratoContaPoupancaResponse.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



